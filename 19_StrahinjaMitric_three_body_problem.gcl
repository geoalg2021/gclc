%Setup
dim 400 200

number t 400
expression dt {0.01}

background 155 155 155

ang3d_picture 0 0 390 190
ang3d_origin 180 70 3.7 0.5


color 122 122 122
ang3d_unit 45
ang3d_drawsystem_p 1 2 2 2 1 2
color 0 0 0

point tmp 1 1 400 400
%cmark tmp
%mark_t tmp
getx iter_number tmp

number current_iter_in_while 0

number M 1 %Mass
number G 2.1 %G force constant

%%NOTE: planets' names have nothing to do with their masses, moving speed or anything :)

expression mass_venus { M * 7 }
expression mass_saturn { M }
expression mass_jupiter { M  }


number venus_x_new 2
number venus_y_new 0
number venus_z_new 0
number jupiter_x_new 0 
number jupiter_y_new 2
number jupiter_z_new 0
number saturn_x_new 0 
number saturn_y_new 0
number saturn_z_new 2

%venus starting parameters

  ang3d_point venus_vector 0 0 0
  ang3d_getx venus_vector_x venus_vector
  ang3d_gety venus_vector_y venus_vector
  ang3d_getz venus_vector_z venus_vector


  expression venus_p_x {venus_vector_x * mass_venus}
  expression venus_p_y {venus_vector_y * mass_venus}
  expression venus_p_z {venus_vector_z * mass_venus}
  
  %data struct for momentum of venus
  ang3d_point venus_p venus_p_x venus_p_y venus_p_z

%%
%saturn starting parameters

  ang3d_point saturn_vector 0.5 0.5 -0.5
  ang3d_getx saturn_vector_x saturn_vector
  ang3d_gety saturn_vector_y saturn_vector
  ang3d_getz saturn_vector_z saturn_vector

  expression saturn_p_x {saturn_vector_x * mass_saturn}
  expression saturn_p_y {saturn_vector_y * mass_saturn}
  expression saturn_p_z {saturn_vector_z * mass_saturn}
  
  %data struct for momentum of saturn
  ang3d_point saturn_p saturn_p_x saturn_p_y saturn_p_z 
%%
%jupiter starting parameters

  %and now we calculate jupiter momentum so that p_jupiter + pvenus + p_saturn = 0
  expression jupiter_p_x {-1 * (saturn_p_x + venus_p_x)}
  expression jupiter_p_y {-1 * (saturn_p_y + venus_p_y)}
  expression jupiter_p_z {-1 * (saturn_p_z + venus_p_z)}

  ang3d_point jupiter_p jupiter_p_x jupiter_p_y jupiter_p_z
%%


%%%%%%%%%%%%%%%%
while {current_iter_in_while < iter_number}
{
  ang3d_point venus venus_x_new venus_y_new venus_z_new
  ang3d_getx venus_x venus
  ang3d_gety venus_y venus
  ang3d_getz venus_z venus

  ang3d_point jupiter jupiter_x_new jupiter_y_new jupiter_z_new
  ang3d_getx jupiter_x jupiter
  ang3d_gety jupiter_y jupiter
  ang3d_getz jupiter_z jupiter

  ang3d_point saturn saturn_x_new saturn_y_new saturn_z_new
  ang3d_getx saturn_x saturn
  ang3d_gety saturn_y saturn
  ang3d_getz saturn_z saturn

  %drawing the vectors
  expression venus_vector_4_draw_x {venus_p_x + venus_x}
  expression venus_vector_4_draw_y {venus_p_y + venus_y}
  expression venus_vector_4_draw_z {venus_p_z + venus_z}
  ang3d_point venus_vector_4_draw venus_vector_4_draw_x venus_vector_4_draw_y venus_vector_4_draw_z

  expression jupiter_vector_4_draw_x {jupiter_p_x + jupiter_x}
  expression jupiter_vector_4_draw_y {jupiter_p_y + jupiter_y}
  expression jupiter_vector_4_draw_z {jupiter_p_z + jupiter_z}
  ang3d_point jupiter_vector_4_draw jupiter_vector_4_draw_x jupiter_vector_4_draw_y jupiter_vector_4_draw_z

  expression saturn_vector_4_draw_x {saturn_p_x + saturn_x}
  expression saturn_vector_4_draw_y {saturn_p_y + saturn_y}
  expression saturn_vector_4_draw_z {saturn_p_z + saturn_z}
  ang3d_point saturn_vector_4_draw saturn_vector_4_draw_x saturn_vector_4_draw_y saturn_vector_4_draw_z
  %%

  %now we have everything set in the initial moment

  %vector from venus to jupiter
  expression v12_x { (jupiter_x - venus_x) }
  expression v12_y { (jupiter_y - venus_y) }
  expression v12_z { (jupiter_z - venus_z) }

  ang3d_point v12 v12_x v12_y v12_z
  expression v12_mag { sqrt(pow(v12_x, 2) + pow(v12_y, 2) + pow(v12_z, 2)) }

  expression v12_norm_x { (v12_x / v12_mag) }
  expression v12_norm_y { (v12_y / v12_mag) }
  expression v12_norm_z { (v12_z / v12_mag) }

  ang3d_point v12_norm v12_norm_x v12_norm_y v12_norm_z

  %vector from venus to saturn
  expression v1p_x { (saturn_x - venus_x) }
  expression v1p_y { (saturn_y - venus_y) }
  expression v1p_z { (saturn_z - venus_z) }

  ang3d_point v1p v1p_x v1p_y v1p_z
  expression v1p_mag { sqrt(pow(v1p_x, 2) + pow(v1p_y, 2) + pow(v1p_z, 2)) }

  expression v1p_norm_x { (v1p_x / v1p_mag) }
  expression v1p_norm_y { (v1p_y / v1p_mag) }
  expression v1p_norm_z { (v1p_z / v1p_mag) }

  ang3d_point v1p_norm v1p_norm_x v1p_norm_y v1p_norm_z

  %vector from jupiter to saturn
  expression v2p_x { (saturn_x - jupiter_x) }
  expression v2p_y { (saturn_y - jupiter_y) }
  expression v2p_z { (saturn_z - jupiter_z) }

  ang3d_point v2p v2p_x v2p_y v2p_z
  expression v2p_mag { sqrt(pow(v2p_x, 2) + pow(v2p_y, 2) + pow(v2p_z, 2)) }

  expression v2p_norm_x { (v2p_x / v2p_mag) }
  expression v2p_norm_y { (v2p_y / v2p_mag) }
  expression v2p_norm_z { (v2p_z / v2p_mag) }

  ang3d_point v2p_norm v2p_norm_x v2p_norm_y v2p_norm_z

  %Now calculating the Grav Force on venus due to jupiter

  expression F12_x { ( G * mass_venus * mass_jupiter * v12_norm_x / pow(v12_mag, 2)) }
  expression F12_y { ( G * mass_venus * mass_jupiter * v12_norm_y / pow(v12_mag, 2)) }
  expression F12_z { ( G * mass_venus * mass_jupiter * v12_norm_z / pow(v12_mag, 2)) }

  ang3d_point F12 F12_x F12_y F12_z

  %Now calculating the Grav Force on jupiter due to venus
  expression F21_x {-1 * F12_x}
  expression F21_y {-1 * F12_y}
  expression F21_z {-1 * F12_z}
  ang3d_point F21 F21_x F21_y F21_z

  %Now calculating the Grav Force on saturn due to venus
  expression F1p_x { ( G * mass_venus * mass_saturn * v1p_norm_x / pow(v1p_mag, 2)) }
  expression F1p_y { ( G * mass_venus * mass_saturn * v1p_norm_y / pow(v1p_mag, 2)) }
  expression F1p_z { ( G * mass_venus * mass_saturn * v1p_norm_z / pow(v1p_mag, 2)) }
  ang3d_point F1p F1p_x F1p_y F1p_z

  %Now calculating the Grav Force on venus due to saturn
  expression Fp1_x {-1 * F1p_x}
  expression Fp1_y {-1 * F1p_y}
  expression Fp1_z {-1 * F1p_z}
  ang3d_point Fp1 Fp1_x Fp1_y Fp1_z

  %Now calculating the Grav Force on saturn due to jupiter
  expression F2p_x { ( G * mass_jupiter * mass_saturn * v2p_norm_x / pow(v2p_mag, 2)) }
  expression F2p_y { ( G * mass_jupiter * mass_saturn * v2p_norm_y / pow(v2p_mag, 2)) }
  expression F2p_z { ( G * mass_jupiter * mass_saturn * v2p_norm_z / pow(v2p_mag, 2)) }
  ang3d_point F2p F2p_x F2p_y F2p_z

  %Now calculating the Grav Force on jupiter due to saturn
  expression Fp2_x {-1 * F2p_x}
  expression Fp2_y {-1 * F2p_y}
  expression Fp2_z {-1 * F2p_z}
  ang3d_point Fp2 Fp2_x Fp2_y Fp2_z

  %now we update momentums 
  expression venus_p_x {( venus_p_x - (F21_x + Fp1_x) * dt)} 
  expression venus_p_y {( venus_p_y - (F21_y + Fp1_y) * dt)} 
  expression venus_p_z {( venus_p_z - (F21_z + Fp1_z) * dt)} 

  expression jupiter_p_x {( jupiter_p_x - (F12_x + Fp2_x) * dt)}
  expression jupiter_p_y {( jupiter_p_y - (F12_y + Fp2_y) * dt)} 
  expression jupiter_p_z {( jupiter_p_z - (F12_z + Fp2_z) * dt)} 

  expression saturn_p_x {( saturn_p_x - (F1p_x + F2p_x) * dt)}
  expression saturn_p_y {( saturn_p_y - (F1p_y + F2p_y) * dt)} 
  expression saturn_p_z {( saturn_p_z - (F1p_z + F2p_z) * dt)} 

  %now update the positions
  expression venus_x_new {(venus_x + venus_p_x * dt / mass_venus )}
  expression venus_y_new {(venus_y + venus_p_y * dt / mass_venus )}
  expression venus_z_new {(venus_z + venus_p_z * dt / mass_venus )}

  ang3d_point venusN venus_x_new venus_y_new venus_z_new

  expression jupiter_x_new {(jupiter_x + jupiter_p_x * dt / mass_jupiter )}
  expression jupiter_y_new {(jupiter_y + jupiter_p_y * dt / mass_jupiter )}
  expression jupiter_z_new {(jupiter_z + jupiter_p_z * dt / mass_jupiter )}

  ang3d_point jupiterN jupiter_x_new jupiter_y_new jupiter_z_new

  expression saturn_x_new {(saturn_x + saturn_p_x * dt / mass_saturn )}
  expression saturn_y_new {(saturn_y + saturn_p_y * dt / mass_saturn )}
  expression saturn_z_new {(saturn_z + saturn_p_z * dt / mass_saturn )}

  ang3d_point saturnN saturn_x_new saturn_y_new saturn_z_new

  expression current_iter_in_while {current_iter_in_while + 1}
}

color 255 0 0
getx xs1 venusN
gety ys1 venusN
expression xs1 {xs1 + 2.5}
point s11 xs1 ys1
circle crc venusN s11
drawcircle crc
fillcircle crc
color 0 0 0

color 0 255 0
getx xs1 jupiterN
gety ys1 jupiterN
expression xs1 {xs1 + 2.5}
point s11 xs1 ys1
circle crc jupiterN s11
drawcircle crc
fillcircle crc
color 0 0 0

color 0 0 255
getx xs1 saturnN
gety ys1 saturnN
expression xs1 {xs1 + 2.5}
point s11 xs1 ys1
circle crc saturnN s11
drawcircle crc
fillcircle crc
color 0 0 0

%cmark venusN
%mark_b venusN
%cmark jupiterN
%mark_b jupiterN
%cmark saturnN
%mark_b saturnN

%draw vector momentum
drawvector venusN venus_vector_4_draw
drawvector jupiterN jupiter_vector_4_draw
drawvector saturn saturn_vector_4_draw

trace venusN 255 0 0
trace saturnN 0 0 255
trace jupiterN 0 255 0


animation_frames t 20






